FROM openjdk:8
WORKDIR '/app'
COPY target/mtg-0.8.jar mtg.jar
CMD ["java", "-jar", "mtg.jar"]
EXPOSE 8080
