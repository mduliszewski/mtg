package com.duliszewski.mtg.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.duliszewski.mtg.entity.Card;
import com.duliszewski.mtg.entity.CardsSearchValues;
import com.duliszewski.mtg.exceptions.ObjectNotFoundException;
import com.duliszewski.mtg.service.CardsApiRetrieverService;
import com.duliszewski.mtg.service.CardsRepositoryService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class CardsController {

    private CardsApiRetrieverService cardsApiRetrieverService;
    private CardsRepositoryService cardsRepositoryService;

    @Autowired
    public CardsController(CardsApiRetrieverService cardsApiRetrieverService, CardsRepositoryService cardsRepositoryService) {
        this.cardsApiRetrieverService = cardsApiRetrieverService;
        this.cardsRepositoryService = cardsRepositoryService;
    }

    @GetMapping("/card")
    public List<Card> getCards(@RequestParam String name,
                               @RequestParam(required = false) String rarity,
                               @RequestParam(required = false) String set,
                               @RequestParam boolean shouldExtendSearch) {

        //@formatter:off
        CardsSearchValues searchValues = CardsSearchValues.builder()
                                                          .name(name)
                                                          .rarity(rarity)
                                                          .set(set)
                                                          .build();
        //@formatter:on

        List<Card> cards = cardsRepositoryService.getCardsByValues(searchValues);

        //if empty or user wants to extend search, try to get card(s) from external API
        if (cards.isEmpty() || shouldExtendSearch) {
            cards = cardsApiRetrieverService.getCardsByMtgApi(searchValues);
        }
        if (cards.isEmpty()) {
            throw new ObjectNotFoundException("No cards were found");
        }
        return cards;
    }

    @GetMapping("/cardPrice")
    public String getCardPrice(@RequestParam String set, @RequestParam String number) {
        String price = cardsApiRetrieverService.getCardPrice(set, number);
        if (price.isEmpty()) {
            throw new ObjectNotFoundException("Price was not found for a given card");
        }
        return cardsApiRetrieverService.getCardPrice(set, number);
    }

    @GetMapping("/cardPicture")
    public String getCardPicture(@RequestParam String set, @RequestParam String number) {
        String cardPictureUrl = cardsApiRetrieverService.getCardPictureUrl(set, number);
        if (cardPictureUrl.isEmpty()) {
            throw new ObjectNotFoundException("Card picture URL was not found for a given card");
        }
        return cardPictureUrl;
    }
}
