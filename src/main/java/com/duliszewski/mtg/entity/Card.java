package com.duliszewski.mtg.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
@Document(collection = "cards")
public class Card {

    private String name;
    private List<String> names;
    private String manaCost;
    private List<String> colors;
    private List<String> colorIdentity;
    private String type;
    private List<String> supertypes;
    private List<String> types;
    private List<String> subtypes;
    private String rarity;
    private String set;
    private String text;
    private String artist;
    private String number;
    private String power;
    private String toughness;
    private String imageUrl;
    private String price;
}
