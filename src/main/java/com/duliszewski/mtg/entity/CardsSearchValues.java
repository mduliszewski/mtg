package com.duliszewski.mtg.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CardsSearchValues {
    private String name;
    private String rarity;
    private String set;
}
