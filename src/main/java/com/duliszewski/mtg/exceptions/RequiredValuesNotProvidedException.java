package com.duliszewski.mtg.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class RequiredValuesNotProvidedException extends RuntimeException {

    public RequiredValuesNotProvidedException() {
        super();
    }

    public RequiredValuesNotProvidedException(String message) {
        super(message);
    }

    public RequiredValuesNotProvidedException(String message, Throwable cause) {
        super(message, cause);
    }

    public RequiredValuesNotProvidedException(Throwable cause) {
        super(cause);
    }
}
