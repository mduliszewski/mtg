package com.duliszewski.mtg.repository;

import com.duliszewski.mtg.entity.Card;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface CardsRepository extends MongoRepository<Card, String> {
    @Query(value = "{name: {$regex: ?0, $options: 'i'}}")
    List<Card> findCardsByName(String name);

    @Query(value = "{name: {$regex: ?0, $options: 'i'}, rarity: {$regex: ?1, $options: 'i'}}")
    List<Card> findCardsByNameAndRarity(String name, String rarity);

    @Query(value = "{name: {$regex: ?0, $options: 'i'}, set: {$regex: ?1, $options: 'i'}}")
    List<Card> findCardsByNameAndSet(String name, String set);

    @Query(value = "{name: {$regex: ?0, $options: 'i'}, rarity: {$regex: ?1, $options: 'i'},  set: {$regex: ?2, $options: 'i'}}")
    List<Card> findCardsByNameRarityAndSet(String name, String rarity, String set);

    List<Card> findByNumberIn(List<String> numbers);
}
