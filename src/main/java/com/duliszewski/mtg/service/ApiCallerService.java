package com.duliszewski.mtg.service;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.duliszewski.mtg.entity.CardsSearchValues;
import com.duliszewski.mtg.exceptions.RequiredValuesNotProvidedException;

import lombok.extern.slf4j.Slf4j;

/**
 * Service that calls external APIs and returns JSON respones.
 */
@Service
@Slf4j
public class ApiCallerService {

    @Value("${api.url.mtg}")
    private String mtgApiUrl;
    @Value("${api.url.scryfall}")
    private String cardsScryfallUrl;

    private RestTemplate restTemplate;

    @Autowired
    public ApiCallerService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /**
     * Retrieves Card data from Scryfall api.
     *
     * @param set        card set
     * @param cardNumber card number
     * @return JSON String containing card data
     */
    String getScryfallCardData(String set, String cardNumber) {
        String url = buildScryfallApiUrl(set, cardNumber);
        ResponseEntity response = new ResponseEntity(HttpStatus.NOT_FOUND);

        try {
            response = restTemplate.getForEntity(url, String.class);
        } catch (Exception e) {
            log.error("Card number {}, not found in scryfall", cardNumber);
        }

        String jsonString = "";
        if (response.hasBody() && response.getBody() != null) {
            jsonString = response.getBody().toString();
        }
        return jsonString;
    }

    /**
     * Retrieves Cards data from Mtg api.
     *
     * @param searchValues Values by which the card is to be found
     * @return JSON string containing matched cards data
     */
    String getMtgApiCardsData(CardsSearchValues searchValues) {
        String url = buildMtgApiUrl(searchValues);

        HttpEntity<String> entity = prepareMtgApiHttpEntity();
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

        String jsonString = "";

        if (response.hasBody() && response.getBody() != null) {
            jsonString = response.getBody();
        }

        return jsonString;
    }

    private String buildMtgApiUrl(CardsSearchValues searchValues) {
        String name = searchValues.getName();
        if (name == null) {
            throw new RequiredValuesNotProvidedException("Part of a name of a card has to be provided!");
        }
        String rarity = searchValues.getRarity();
        String set = searchValues.getSet();

        String url = mtgApiUrl + name;

        if (rarity != null) {
            url += "&rarity=" + rarity;
        }
        if (set != null) {
            url += "&set=" + set;
        }
        return url;
    }

    private String buildScryfallApiUrl(String set, String cardNumber) {
        return cardsScryfallUrl + set.toLowerCase() + "/" + cardNumber;
    }

    private HttpEntity<String> prepareMtgApiHttpEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");

        return new HttpEntity<>("parameters", headers);
    }
}
