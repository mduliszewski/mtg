package com.duliszewski.mtg.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.duliszewski.mtg.entity.Card;
import com.duliszewski.mtg.entity.CardsSearchValues;

import lombok.extern.slf4j.Slf4j;

/**
 * Service that retrieves cards data based on provided values.
 */
@Service
@Slf4j
public class CardsApiRetrieverService {

    private final CardsRepositoryService cardsRepositoryService;
    private final ApiCallerService apiCallerService;
    private final CardsJsonParserService cardsJsonParserService;

    @Autowired
    public CardsApiRetrieverService(ApiCallerService apiCallerService, CardsRepositoryService cardsRepositoryService,
        CardsJsonParserService cardsJsonParserService) {
        this.apiCallerService = apiCallerService;
        this.cardsRepositoryService = cardsRepositoryService;
        this.cardsJsonParserService = cardsJsonParserService;
    }

    /**
     * Retrieves MTG cards from MTG Api response in json representation, based on up to 3 arguments: 1. Name, 2. Rarity, 3. Card Set.
     *
     * @param searchValues up to 3 arguments: 1. Name, 2. Rarity, 3. Card Set (Name is required)
     * @return List of {@link Card} parsed from json
     */
    public List<Card> getCardsByMtgApi(CardsSearchValues searchValues) {
        String jsonString = apiCallerService.getMtgApiCardsData(searchValues);
        List<Card> receivedCards = cardsJsonParserService.parseCardsFromMtgApiResponse(jsonString);

        cardsRepositoryService.saveCardsIfNotExist(receivedCards);

        return receivedCards;
    }

    /**
     * Retrieves Card price from Scryfall api response json.
     *
     * @param set        Card's set
     * @param cardNumber Card's number
     * @return Card's price
     */
    public String getCardPrice(String set, String cardNumber) {
        String jsonString = apiCallerService.getScryfallCardData(set, cardNumber);

        return cardsJsonParserService.parseCardPriceFromScryfallApiResponse(jsonString);
    }

    /**
     * Parses Card picture from Scryfall api response json.
     *
     * @param set        Card's set
     * @param cardNumber Card's number
     * @return Card's picture url
     */
    public String getCardPictureUrl(String set, String cardNumber) {
        String jsonString = apiCallerService.getScryfallCardData(set, cardNumber);

        return cardsJsonParserService.parseCardPictureUrlFromScryfallApiResponse(jsonString);
    }
}
