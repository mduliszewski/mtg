package com.duliszewski.mtg.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.duliszewski.mtg.entity.Card;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * Service that parses JSON string responses to Card values.
 */
@Service
@Slf4j
public class CardsJsonParserService {

    private static final ObjectMapper mapper = new ObjectMapper();

    public List<Card> parseCardsFromMtgApiResponse(String mtgJson) {
        List<Card> receivedCards = new ArrayList<>();

        try {
            mapper.readTree(mtgJson)
                .get("cards")
                .forEach(record -> {
                    Card card = mapper.convertValue(record, Card.class);
                    receivedCards.add(card);
                });
        } catch (IOException e) {
            log.error("Error parsing JSON response", e);
        }

        return receivedCards;
    }

    public String parseCardPriceFromScryfallApiResponse(String scryfallJson) {
        String usd = "";

        if (!scryfallJson.isEmpty()) {
            try {
                JsonNode pricesNode = mapper.readTree(scryfallJson).get("prices");
                usd = pricesNode.get("usd").toString();

            } catch (NullPointerException | IOException ex) {
                log.error("No price info for a card", ex);
            }
        }

        return usd;
    }

    public String parseCardPictureUrlFromScryfallApiResponse(String scryfallJson) {
        String pictureUrl = "";

        if (!scryfallJson.isEmpty()) {
            try {
                JsonNode imagesNode = mapper
                    .readTree(scryfallJson)
                    .get("image_uris");

                pictureUrl = imagesNode.get("border_crop").toString();

                if (pictureUrl == null) {
                    pictureUrl = imagesNode.get("normal").toString();
                }

            } catch (IOException ex) {
                log.error("No picture for this card", ex);
            }
        }

        return pictureUrl;
    }
}
