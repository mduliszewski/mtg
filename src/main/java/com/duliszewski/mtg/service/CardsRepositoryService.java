package com.duliszewski.mtg.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.duliszewski.mtg.entity.Card;
import com.duliszewski.mtg.entity.CardsSearchValues;
import com.duliszewski.mtg.exceptions.RequiredValuesNotProvidedException;
import com.duliszewski.mtg.repository.CardsRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * Service that retrieves Card data from repository.
 */
@Service
@Slf4j
public class CardsRepositoryService {

    private CardsRepository cardsRepository;

    @Autowired
    public CardsRepositoryService(CardsRepository cardsRepository) {
        this.cardsRepository = cardsRepository;
    }

    /**
     * Finds all cards that match provided values.
     *
     * @param cardsSearchValues Values of cards that are to be found
     * @return a List of found cards
     */
    public List<Card> getCardsByValues(CardsSearchValues cardsSearchValues) {

        String name = cardsSearchValues.getName();
        if (name == null) {
            throw new RequiredValuesNotProvidedException("Part of a name of a card has to be provided!");
        }
        String rarity = cardsSearchValues.getRarity();
        String set = cardsSearchValues.getSet();

        if (rarity == null && set == null) {
            return cardsRepository.findCardsByName(name);
        } else if (set == null) {
            return cardsRepository.findCardsByNameAndRarity(name, rarity);
        } else if (rarity == null) {
            return cardsRepository.findCardsByNameAndSet(name, set);
        } else {
            return cardsRepository.findCardsByNameRarityAndSet(name, rarity, set);
        }
    }

    public void saveCardsIfNotExist(List<Card> cards) {
        List<String> cardsNumbers = cards.stream().map(card -> card.getNumber()).collect(Collectors.toList());
        List<Card> existingCards = cardsRepository.findByNumberIn(cardsNumbers);

        List<Card> notFoundCards = cards.stream().filter(card -> !existingCards.contains(card)).collect(Collectors.toList());

        cardsRepository.saveAll(notFoundCards);
    }

    public void clearAll() {
        cardsRepository.deleteAll();
    }
}
