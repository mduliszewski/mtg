package com.duliszewski.mtg.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.duliszewski.mtg.crosscutting.IntegrationTestPreparer;
import com.duliszewski.mtg.entity.Card;

/**
 * End to end app backend tests.
 */

public class CardsControllerIntegrationTest extends IntegrationTestPreparer {

    @Autowired
    private CardsController cardsController;

    @Test
    void getCardsReturnsCardsFromRepo() {
        List<Card> response = cardsController.getCards("Dark", "rare", null, false);

        assertThat(response).isNotNull();
        assertThat(response).isNotEmpty();
        assertThat(response).hasSize(4);
        assertThat(response).extracting("name", "rarity")
            .contains(
                tuple(ADARKAR_WASTES, RARE),
                tuple(DARK_IMPOSTOR, RARE),
                tuple(ADARKAR_VALKYRIE, RARE),
                tuple(DARK_DEPTHS, RARE)
            );
    }

    @Test
    void getCardsReturnsCardsFromRepoAndCallsApi() {
        List<Card> response = cardsController.getCards("Dark", "rare", null, true);

        assertThat(response).isNotNull();
        assertThat(response).isNotEmpty();
        assertThat(response).size().isBetween(70, 180);
        assertThat(response).extracting("name", "rarity")
            .contains(
                tuple(ADARKAR_WASTES, RARE),
                tuple(DARK_IMPOSTOR, RARE),
                tuple(ADARKAR_VALKYRIE, RARE),
                tuple(DARK_DEPTHS, RARE)
            );
    }

    @Test
    void getCardPictureReturnsPictureUrl() {
        String response = cardsController.getCardPicture("10E", "347");

        assertThat(response).isNotNull();
        assertThat(response)
            .isEqualTo("\"https://img.scryfall.com/cards/border_crop/front/f/3/f31cf014-7ac9-428b-9ce9-8ba5ebfdd252.jpg?1562557714\"");
    }

    @Test
    void getCardPriceReturnsCardPrice() {
        String response = cardsController.getCardPrice("10E", "347");

        assertThat(response).isNotNull();
        assertThat(response).isNotEmpty();
    }
}
