package com.duliszewski.mtg.controller;

import static java.util.Arrays.asList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.duliszewski.mtg.entity.Card;
import com.duliszewski.mtg.entity.CardsSearchValues;
import com.duliszewski.mtg.service.CardsApiRetrieverService;
import com.duliszewski.mtg.service.CardsRepositoryService;

import static org.hamcrest.Matchers.is;

@WebMvcTest(CardsController.class)
public class CardsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CardsApiRetrieverService cardsApiRetrieverService;

    @MockBean
    private CardsRepositoryService cardsRepositoryService;

    private static final String cardName = "CardName";
    private static final  CardsSearchValues cardsSearchValues =  CardsSearchValues.builder()
                                                                                  .name(cardName)
                                                                                  .build();

    @Test
    public void getCards_ReturnsCardsFromRepository() throws Exception {
        //given-when
        when(cardsRepositoryService.getCardsByValues(cardsSearchValues)).thenReturn(
            asList(Card.builder().name(cardName).build())
        );

        //when-then
        mockMvc.perform(
            MockMvcRequestBuilders.get("/api/card")
            .param("name", cardName)
            .param("shouldExtendSearch", "false")
        )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[0].name", is(cardName)));

        verify(cardsRepositoryService, times(1)).getCardsByValues(cardsSearchValues);
        verify(cardsApiRetrieverService, times(0)).getCardsByMtgApi(cardsSearchValues);
    }

    @Test
    void getCards_ReturnsCardsFromApi_ShouldExtendSearchBecauseNoCardsWereFound() throws Exception {
        //given-when
        when(cardsApiRetrieverService.getCardsByMtgApi(cardsSearchValues)).thenReturn(
            asList(Card.builder().name(cardName).build())
        );

        //when-then
        mockMvc.perform(
            MockMvcRequestBuilders.get("/api/card")
                .param("name", cardName)
                .param("shouldExtendSearch", "false")
        )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[0].name", is(cardName)));


        verify(cardsRepositoryService, times(1)).getCardsByValues(cardsSearchValues);
        verify(cardsApiRetrieverService, times(1)).getCardsByMtgApi(cardsSearchValues);
    }

    @Test
    void getCards_NoCardsFound_ShouldReturnStatusNotFound() throws Exception {
        //given-when-then
        mockMvc.perform(
            MockMvcRequestBuilders.get("/api/card")
                .param("name", cardName)
                .param("shouldExtendSearch", "false")
        )
           .andExpect(status().isNotFound());


        verify(cardsRepositoryService, times(1)).getCardsByValues(cardsSearchValues);
        verify(cardsApiRetrieverService, times(1)).getCardsByMtgApi(cardsSearchValues);
    }

}
