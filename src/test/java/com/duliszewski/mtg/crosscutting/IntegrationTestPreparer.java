package com.duliszewski.mtg.crosscutting;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.duliszewski.mtg.entity.Card;
import com.duliszewski.mtg.service.CardsRepositoryService;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegrationTestPreparer {

    protected static final String RARE = "Rare";
    protected static final String ADARKAR_WASTES = "Adarkar Wastes";
    protected static final String DARK_IMPOSTOR = "Dark Impostor";
    protected static final String ADARKAR_VALKYRIE = "Adarkar Valkyrie";
    protected static final String DARK_DEPTHS = "Dark Depths";
    protected static final String SET = "10e";

    @Autowired
    private CardsRepositoryService cardsRepositoryService;

    @PostConstruct
    void insertDataToDb() {
        cardsRepositoryService.clearAll();

        List<Card> testCards = new ArrayList<>();
        testCards.add(Card.builder().name(ADARKAR_WASTES).rarity(RARE).number("1").build());
        testCards.add(Card.builder().name(DARK_IMPOSTOR).rarity(RARE).number("2").set(SET).build());
        testCards.add(Card.builder().name(ADARKAR_VALKYRIE).rarity(RARE).number("3").build());
        testCards.add(Card.builder().name(DARK_DEPTHS).rarity(RARE).number("4").build());

        cardsRepositoryService.saveCardsIfNotExist(testCards);
    }
}
