package com.duliszewski.mtg.repositories;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.duliszewski.mtg.crosscutting.IntegrationTestPreparer;
import com.duliszewski.mtg.entity.Card;
import com.duliszewski.mtg.repository.CardsRepository;

public class CardsRepositoryIntegrationTest extends IntegrationTestPreparer {

    @Autowired
    private CardsRepository cardsRepository;

    @Test
    void findCardsByNameIgnoringCaseReturnsCard() {
        List<Card> result = cardsRepository.findCardsByName("DArK iMPOSTOR");

        assertThat(result).isNotNull();
        assertThat(result).isNotEmpty();
        assertThat(result).extracting("number", "name")
            .contains(tuple("2", DARK_IMPOSTOR));
    }

    @Test
    void findCardsByNameAndRarityIgnoringCaseReturnsCards() {
        List<Card> result = cardsRepository.findCardsByNameAndRarity("DARK IMPoSTOR", "rArE");

        assertThat(result).isNotNull();
        assertThat(result).isNotEmpty();
        assertThat(result).extracting("number", "name", "rarity")
            .contains(tuple("2", DARK_IMPOSTOR, RARE));
    }

    @Test
    void findAllByNumbersShouldReturnChosenCards() {
        List<Card> result = cardsRepository.findByNumberIn(asList("1", "2", "3", "4"));

        assertThat(result).isNotNull();
        assertThat(result).isNotEmpty();
        assertThat(result).extracting("number", "name")
                          .contains(tuple("1", ADARKAR_WASTES),
                                    tuple("2", DARK_IMPOSTOR),
                                    tuple("3", ADARKAR_VALKYRIE),
                                    tuple("4", DARK_DEPTHS)
                          );
    }

    @Test
    void findCardsByNameAndSetIgnoringCaseReturnsCards() {
        List<Card> result = cardsRepository.findCardsByNameAndSet("dARK IMPoSTOR", "10E");
        assertThat(result).isNotNull();
        assertThat(result).isNotEmpty();
        assertThat(result).extracting("number", "name", "set")
            .contains(tuple("2", DARK_IMPOSTOR, SET));
    }
}
