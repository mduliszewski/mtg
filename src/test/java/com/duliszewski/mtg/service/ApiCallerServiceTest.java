package com.duliszewski.mtg.service;

import com.duliszewski.mtg.entity.CardsSearchValues;
import com.duliszewski.mtg.exceptions.RequiredValuesNotProvidedException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ApiCallerServiceTest {

    private static RestTemplate restTemplate;

    private static ApiCallerService apiCallerService;

    private static final String cardName = "exampleName";
    private static final String cardRarity = "exampleRarity";
    private static final String cardSet = "exampleSet";
    private static final String cardNumber = "1";

    @BeforeAll
    static void setUp(@Mock RestTemplate restTemplateMock) {
        restTemplate = restTemplateMock;
        apiCallerService = new ApiCallerService(restTemplate);
        ReflectionTestUtils.setField(apiCallerService, "mtgApiUrl", "https://api.magicthegathering.io/v1/cards?name=");
        ReflectionTestUtils.setField(apiCallerService, "cardsScryfallUrl", "https://api.scryfall.com/cards/");
    }

    @Test
    void getMtgApiCardDataReturnsJsonString() {
        //@formatter:off
        CardsSearchValues cardSearchValues = CardsSearchValues.builder()
                                                              .name(cardName)
                                                              .rarity(cardRarity)
                                                              .set(cardSet)
                                                              .build();
        //@formatter:on

        //Check if method will be invoked with proper url string
        String createdUrl = "https://api.magicthegathering.io/v1/cards?name=exampleName&rarity=exampleRarity&set=exampleSet";

        when(restTemplate.exchange(eq(createdUrl), eq(HttpMethod.GET), any(HttpEntity.class), eq(String.class))).thenReturn(ResponseEntity.ok("MTG API response json"));

        //given-when
        String result = apiCallerService.getMtgApiCardsData(cardSearchValues);

        //then
        assertThat(result).contains("MTG API response json");

    }

    @Test
    void getMtgApiCardDataThrowsException() {
        //given-when-then
        assertThatExceptionOfType(RequiredValuesNotProvidedException.class)
                .isThrownBy(() -> apiCallerService.getMtgApiCardsData(CardsSearchValues.builder().build()))
                .withMessage("Part of a name of a card has to be provided!");
    }

    @Test
    void getScryfallCardDataReturnsCardsJsonString() {
        //Check if the method will be invoked with proper url string
        //given
        String createdUrl = "https://api.scryfall.com/cards/exampleset/1";
        when(restTemplate.getForEntity(createdUrl, String.class)).thenReturn(ResponseEntity.ok("Scryfall API response json"));

        //given-when
        String result = apiCallerService.getScryfallCardData(cardSet, cardNumber);

        //then
        assertThat(result).contains("Scryfall API response json");
    }
}
