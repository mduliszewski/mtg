package com.duliszewski.mtg.service;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.duliszewski.mtg.entity.Card;
import com.duliszewski.mtg.entity.CardsSearchValues;

@ExtendWith(MockitoExtension.class)
class CardsApiRetrieverServiceTest {

    private static final String cardName = "Counterspell";
    private static final String cardRarity = "Mythic";
    private static final String cardSet = "MP2";
    private static CardsApiRetrieverService retrieverService;
    private static ApiCallerService apiCallerService;
    private static CardsRepositoryService cardsRepositoryService;
    private static CardsJsonParserService cardsJsonParserService;

    @BeforeAll
    static void setUp(@Mock ApiCallerService apiCallerServiceMock, @Mock CardsRepositoryService cardsRepositoryServiceMock,
        @Mock CardsJsonParserService cardsJsonParserServiceMock) {

        apiCallerService = apiCallerServiceMock;
        cardsRepositoryService = cardsRepositoryServiceMock;
        cardsJsonParserService = cardsJsonParserServiceMock;

        retrieverService = new CardsApiRetrieverService(apiCallerService, cardsRepositoryService, cardsJsonParserService);
    }

    @Test
    void getCardsByMtgApiShouldParseJsonResponseToCards() {
        //given
        String jsonResponse = "exampleResponse";
        List<Card> parsedCards = asList(Card.builder()
            .name(cardName)
            .rarity(cardRarity)
            .set(cardSet)
            .build());

        //@formatter:off
        CardsSearchValues cardsSearchValues = CardsSearchValues.builder()
                                                               .name(cardName)
                                                               .rarity(cardRarity)
                                                               .set(cardSet)
                                                               .build();
        //@formatter:on
        when(apiCallerService.getMtgApiCardsData(cardsSearchValues)).thenReturn(jsonResponse);
        when(cardsJsonParserService.parseCardsFromMtgApiResponse(jsonResponse)).thenReturn(parsedCards);

        //when
        List<Card> result = retrieverService.getCardsByMtgApi(cardsSearchValues);

        assertThat(result)
            .extracting("name", "rarity", "set")
            .contains(tuple(cardName, cardRarity, cardSet));

        verify(cardsRepositoryService, times(1)).saveCardsIfNotExist(parsedCards);
    }
}