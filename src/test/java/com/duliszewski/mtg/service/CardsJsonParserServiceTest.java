package com.duliszewski.mtg.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.duliszewski.mtg.entity.Card;

@ExtendWith(MockitoExtension.class)
class CardsJsonParserServiceTest {

    private static CardsJsonParserService cardsJsonParserService;

    @BeforeAll
    static void setUp() {
        cardsJsonParserService = new CardsJsonParserService();
    }

    @Test
    void parseCardsFromMtgApiResponseParsesResponseToACard() {
        String mtgApiResponse = "{\"cards\":[{\"name\":\"Counterspell\",\"manaCost\":\"{U}{U}\",\"cmc\":2.0,\"colors\":[\"Blue\"],"
            + "\"colorIdentity\":[\"U\"],\"type\":\"Instant\",\"supertypes\":[],\"types\":[\"Instant\"],\"subtypes\":[],\"rarity\":\"Mythic\","
            + "\"set\":\"MP2\",\"setName\":\"Amonkhet Invocations\",\"text\":\"Counter target spell.\",\"artist\":\"Chase Stone\","
            + "\"number\":\"10\",\"layout\":\"normal\",\"multiverseid\":429869,\"imageUrl\":\"http://gatherer.wizards.com/Handlers/Image"
            + ".ashx?multiverseid=429869&type=card\",\"rulings\":[],\"foreignNames\":[],\"printings\":[\"2ED\",\"3ED\",\"4BB\",\"4ED\",\"5ED\","
            + "\"6ED\",\"7ED\",\"A25\",\"BRB\",\"BTD\",\"CED\",\"CEI\",\"DD2\",\"EMA\",\"F05\",\"FBB\",\"G00\",\"ICE\",\"JVC\",\"LEA\",\"LEB\","
            + "\"ME2\",\"ME4\",\"MMQ\",\"MP2\",\"PLGM\",\"PRM\",\"PTC\",\"S99\",\"SS1\",\"SUM\",\"TMP\",\"TPR\",\"VMA\",\"WC00\",\"WC01\",\"WC02\","
            + "\"WC97\",\"WC98\"],\"originalText\":\"Counter target spell.\",\"originalType\":\"Instant\","
            + "\"legalities\":[{\"format\":\"Commander\",\"legality\":\"Legal\"},{\"format\":\"Duel\",\"legality\":\"Legal\"},"
            + "{\"format\":\"Legacy\",\"legality\":\"Legal\"},{\"format\":\"Pauper\",\"legality\":\"Legal\"},{\"format\":\"Vintage\","
            + "\"legality\":\"Legal\"}],\"id\":\"7d746fa3-0fb8-5c58-a9b5-6e70034240a5\"}]}";

        List<Card> result = cardsJsonParserService.parseCardsFromMtgApiResponse(mtgApiResponse);

        assertThat(result).isNotNull();
        assertThat(result).hasSize(1);
        assertThat(result)
            .extracting("name", "rarity", "set")
            .contains(tuple("Counterspell", "Mythic", "MP2"));
    }
}