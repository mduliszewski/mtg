package com.duliszewski.mtg.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.duliszewski.mtg.entity.Card;
import com.duliszewski.mtg.entity.CardsSearchValues;
import com.duliszewski.mtg.repository.CardsRepository;

@ExtendWith(MockitoExtension.class)
class CardsRepositoryServiceTest {

    private static final String cardName = "Counterspell";
    private static final String cardRarity = "Mythic";
    private static final String cardSet = "MP2";
    private CardsRepositoryService cardsRepositoryService;
    private CardsRepository cardsRepository;

    @BeforeEach
    void setUp(@Mock CardsRepository cardsRepositoryMock) {
        cardsRepository = cardsRepositoryMock;
        cardsRepositoryService = new CardsRepositoryService(cardsRepository);
    }

    @Test
    void getCardsByValuesCallsOnlyFindCardsByNameRarityAndSetMethod() {
        //given-when
        cardsRepositoryService.getCardsByValues(CardsSearchValues.builder()
                                                                 .name(cardName)
                                                                 .rarity(cardRarity)
                                                                 .set(cardSet)
                                                                 .build());

        //then
        verify(cardsRepository, times(1)).findCardsByNameRarityAndSet(cardName, cardRarity, cardSet);
        verify(cardsRepository, times(0)).findCardsByName(cardName);
        verify(cardsRepository, times(0)).findCardsByNameAndRarity(cardName, cardRarity);
        verify(cardsRepository, times(0)).findCardsByNameAndSet(cardName, cardSet);
    }

    @Test
    void getCardsByValuesCallsOnlyFindCardsByNameMethod() {
        //given-when
        cardsRepositoryService.getCardsByValues(CardsSearchValues.builder()
                                                                 .name(cardName)
                                                                 .build());

        //then
        verify(cardsRepository, times(0)).findCardsByNameRarityAndSet(cardName, cardRarity, cardSet);
        verify(cardsRepository, times(1)).findCardsByName(cardName);
        verify(cardsRepository, times(0)).findCardsByNameAndRarity(cardName, cardRarity);
        verify(cardsRepository, times(0)).findCardsByNameAndSet(cardName, cardSet);
    }

    @Test
    void getCardsByValuesCallsOnlyFindCardsByNameAndRarityMethod() {
        //given-when
        List<Card> result = cardsRepositoryService.getCardsByValues(CardsSearchValues.builder()
                                                                                     .name(cardName)
                                                                                     .rarity(cardRarity)
                                                                                     .build());

        //then
        verify(cardsRepository, times(0)).findCardsByNameRarityAndSet(cardName, cardRarity, cardSet);
        verify(cardsRepository, times(0)).findCardsByName(cardName);
        verify(cardsRepository, times(1)).findCardsByNameAndRarity(cardName, cardRarity);
        verify(cardsRepository, times(0)).findCardsByNameAndSet(cardName, cardSet);
    }

    @Test
    void getCardsByValuesCallsOnlyFindCardsByNameAndSetMethod() {
        //given-when
        List<Card> result = cardsRepositoryService.getCardsByValues(CardsSearchValues.builder()
                                                                                     .name(cardName)
                                                                                     .set(cardSet)
                                                                                     .build());

        //then
        verify(cardsRepository, times(0)).findCardsByNameRarityAndSet(cardName, cardRarity, cardSet);
        verify(cardsRepository, times(0)).findCardsByName(cardName);
        verify(cardsRepository, times(0)).findCardsByNameAndRarity(cardName, cardRarity);
        verify(cardsRepository, times(1)).findCardsByNameAndSet(cardName, cardSet);
    }
}